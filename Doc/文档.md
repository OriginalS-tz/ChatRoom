## 需求分析

基于TCP/IP的在线聊天系统的需求如下

**服务器端**

- 使用c++

- 拥有GUI
- 可自定义配置
- 能快速处理大量连接
- 可对用户进行管理

**客户端**

- 使用java

- 拥有基本的聊天功能
- 可自定义配置

## 概要设计

服务器端的概要设计如下

GUI采用QT制作，Socket使用Unix Socket API

对于大量的用户连接，采用线程池的方式进行应对

使用JSON进行自定义配置，如配置端口之类，是否启用用户.....

数据库采用简单易用的Sqlite，用户可以随时随地搭建起一个简单的聊天系统

## 详细设计

- UI
- 网络模块
- 线程池
- 连接池
- 数据库模块
- Json模块

### UI

为用户提供一个易操作的图形界面

### 网络模块

网络模块主要是负责接受客户端的连接，然后将其交给子线程处理

### 线程池

线程池主要是提供一个大小合理的线程池，使用者可以将任务提交给线程池让线程去完成

### 连接池

连接池模块主要是放置多个已经准备好的数据库连接，即拿即用，节省了连接数据库的时间

### 数据库模块

数据库模块主要是提供数据库操作的接口

### JSON模块

JSON模块主要是提供读取和改变json文件的接口，其本质是一个简单的递归下降的JSON解析器

解析规则如下

```
obj = {obj | data}
data = key : value
value = content | obj | array
array = `[`content`]`
key = text
content = (text | number)*
text = `"`([a-zA-Z0-9] | symbol)*`"`
number = digit*
```

## Redis缓存设计

Redis 缓存设计

缓存项目：存放在缓存中以备客户端提出请求

- 好友列表
- 聊天记录

公有聊天记录list,持久化+缓存200条

public_list (record )

record=user, date,msg

私有聊天记录,持久化+缓存

private_list(record)

record=user1,user2,date,msg



好友列表，一旦请求则放入缓存，暂存时间为10小时

friends : set(users...)

## 数据库设计

```
create table user 
(
id integer primary key autoincrement, 
user_name char(30) not null,
user_pw char(20) not null
);

create table history
(
user_id integer not null,
msg char(200) not null,
date char(20) not null
);

create table personal_history
(
user_id integer primary key,
msg_id integer not null,
foreign key (msg_id) references personal_msg(id) on delete cascade on update cascade
);

create table personal_msg
(
id integer primary key,
msg char(200) not null
)
```

## 遇到的问题

### 大小端问题

http://www.cnblogs.com/chuwzh/articles/2715761.html

只有多字节类型才考虑大小端问题

C/C++语言编写的程序里数据存储顺序是跟编译平台所在的CPU相关的，而JAVA编写的程序则唯一采用big endian方式来存储数据。试想，如果你用C/C++语言在x86平台下编写的程序跟别人的JAVA程序互通时会产生什么结果？就拿上面的0x12345678来说，你的程序传递给别人的一个数据，将指向0x12345678的指针传给了JAVA程序，由于JAVA采取big endian方式存储数据，很自然的它会将你的数据翻译为0x78563412。什么？竟然变成另外一个数字了？是的，就是这种后果。因此，在你的C程序传给JAVA程序之前有必要进行字节序的转换工作。

所以在用C/C++写通信程序时，在发送数据前务必用htonl和htons去把整型和短整型的数据进行从主机字节序到网络字节序的转换，而接收数据后对于整型和短整型数据则必须调用ntohl和ntohs实现从网络字节序到主机字节序的转换。如果通信的一方是JAVA程序、一方是C/C++程序时，则需要在C/C++一侧使用以上几个方法进行字节序的转换，而JAVA一侧，则不需要做任何处理，因为JAVA字节序与网络字节序都是BIG-ENDIAN，只要C/C++一侧能正确进行转换即可（发送前从主机序到网络序，接收时反变换）。如果通信的双方都是JAVA，则根本不用考虑字节序的问题了



https://www.cnblogs.com/orlion/p/6104204.html



在C/C++写网络程序的时候，往往会遇到字节的网络顺序和主机顺序的问题。这是就可能用到htons(), ntohl(), ntohs()，htons()这4个函数。

网络字节顺序与本地字节顺序之间的转换函数：

```
    htonl()--"Host to Network Long" 32
    ntohl()--"Network to Host Long"
    htons()--"Host to Network Short" 16
    ntohs()--"Network to Host Short"    
```

　　之所以需要这些函数是因为计算机数据表示存在两种字节顺序：NBO与HBO

### 多线程读写导致的错误：

在自线程试图使用指针改变主线程中的某个数据，但是由于读写函数不具备原子性，导致修改失败

### SIGPIPE信号未捕获导致程序退出，exit code =141：

在对端关闭了socket时试图发送报文，发送的报文会导致对端发送RST报文, 因为对端的socket已经调用了close, 完全关闭, 既不发送, 也不接收数据. 所以, 
第二次调用write方法(假设在收到RST之后), 会生成**SIGPIPE**信号, 导致进程退出.

为了避免进程退出, 可以捕获**SIGPIPE**信号, 或者忽略它, 给它设置**SIG_IGN**信号处理函数:

**signal**(**SIGPIPE**, **SIG_IGN**);

### 使用太多内存被系统强制杀死，OOM-Killer，exit code=137

https://blog.csdn.net/hunanchenxingyu/article/details/26271293

Process finished with exit code 137 (interrupted by signal 9: SIGKILL) 

Linux : /var/log/messages

因为线程多于CPU数会降低程序的运行速度，GCD将通过dispatch_get_global_queue 调用可以接受的一个标志(DISPATCH_QUEUE_OVERCOMMIT)来支持overcommit，但是苹果的文档掩盖了这个事实，宣传这个标志必须为0