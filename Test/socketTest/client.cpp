#include <iostream>
#include <unistd.h>    //use fork,exec, read, write, close
#include <arpa/inet.h> //use the some struct

#define MAX 10
class Clinet
{
  private:
    char receive[100];
    char send[100];
    sockaddr_in servAddr;

  public:
    Clinet();
    void connecting();
};

Clinet::Clinet()
{
    bzero(&servAddr, sizeof(servAddr));
    servAddr.sin_family = AF_INET;                   //使用IPv4地址
    servAddr.sin_addr.s_addr = inet_addr("0.0.0.0"); //具体的IP地址,服务器的ip地址
    servAddr.sin_port = htons(10086);                 //端口，服务器开放的端口
}

//void Clinet::connecting() {
//    bzero(&send, sizeof(send));
//    bzero(&receive, sizeof(send));
//
//    int maxfdp1, stdineof;
//    fd_set rset;
//
//    int n;
//    int sock;
//    if( (sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
//        std::cout << "create socket fail";
//        exit(0);
//    } else {
//        std::cout << "socket ready" << std::endl;
//    }
//
//    if(connect(sock, (struct sockaddr*)&servAddr, sizeof(servAddr)) < 0){
//        std::cout << "can not connect" << std::endl;
//        exit(0);
//    } else {
//        std::cout << "connect ready" << std::endl;
//    }
//
//    stdineof = 0;
//
//    FD_ZERO(&rset);
//	for ( ; ; ) {
//		if (stdineof == 0)
//			FD_SET(fileno(stdin), &rset);
//        else
//            break;
//        FD_SET(sock, &rset);
//        maxfdp1 = std::max(fileno(stdin), sock) + 1;
//        select(maxfdp1, &rset, NULL, NULL, NULL);
//
//        if (FD_ISSET(sock, &rset)) {	/* socket is readable */
//            if ( (n = NET::readn(sock, receive, MAX)) == 0) {
//                if (stdineof == 1)
//                    return;		/* normal termination */
//                else
//                    std::cerr << "str_cli: server terminated prematurely";
//            }
//            NET::writen(fileno(stdout), receive, n);
//        }
//
//        if (FD_ISSET(fileno(stdin), &rset)) {  /* input is readable */
//            if ( (n = NET::readn(fileno(stdin), receive, MAX)) == 0) {
//                stdineof = 1;
//                shutdown(sock, SHUT_WR);	/* send FIN */
//                FD_CLR(fileno(stdin), &rset);
//                continue;
//            }
//            NET::writen(sock, receive, n);
//        }
//    }
//}
//

void Clinet::connecting()
{
    bzero(&send, sizeof(send));
    bzero(&receive, sizeof(send));

    int sock = -1;
    while (1)
    {
        //创建套接字
        if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
        {
            std::cout << "creat socket fail";
            exit(0);
        }
        //读取服务器传回的数据
        if (connect(sock, (struct sockaddr *)&servAddr, sizeof(servAddr)) < 0)
        {
            std::cout << "can not connect" << std::endl;
            exit(0);
        }

        //        std::cin.get(send, 100); //导致服务器无限读取
        std::cin.getline(send, 100); //输入要发送的数据

        //write(sock, send, 100);
        //write(sock, send, 100);
        //write(sock, send, 100);
        int len = 3;
        len = htons(len);
        write(sock, &len, 4);
        write(sock, "1#Tom", 5);
        read(sock, receive, 100);
        //NET::writen(sock, send, sizeof(send)); //发送数据
        //NET::readn(sock, receive, sizeof(receive)); //接收数据

        std::cout << "message :" << receive << std::endl;
        bzero(&send, sizeof(send));
        bzero(&receive, sizeof(send));
        close(sock); //关闭套接字
    }
}

int main()
{
    Clinet myClinet;
    std::string MyIp;
    myClinet.connecting();
    return 0;
}
