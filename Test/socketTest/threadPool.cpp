#include <mutex>
#include <condition_variable>
#include <memory>
#include <atomic>
#include <thread>
#include <functional>
#include <vector>
#include <queue>
#include <string>

#include <iostream>


template<typename T>
class ThreadSafe_Queue
{
public:
    void push(const T &msg)
    {
        std::lock_guard<std::mutex> lock(mut);
        queue.push(std::move(msg));
    }

    bool try_pop(T &get)
    {
        std::lock_guard<std::mutex> lock(mut);
        if (queue.empty())
        {
            return false;
        }
        get = std::move(queue.front());
        queue.pop();
        return true;
    }

    bool empty() const
    {
        std::lock_guard<std::mutex> lock(mut);
        return queue.empty();
    }

private:
    mutable std::mutex mut;
    std::queue<T> queue;
};


class join_threads
{
    std::vector<std::thread> &threads;
public:
	explicit join_threads(std::vector<std::thread> &threads_) : threads(threads_){}
	
    ~join_threads()
	{
	    for(unsigned long i = 0 ; i < threads.size();++i)
	    {
	        if(threads[i].joinable())
	        	threads[i].join();
	    }
	}
};

class ServerThreadPool
{
public:
    ServerThreadPool();
    ~ServerThreadPool()
    {
        done=true;
    }

    template<typename FunctionType>
    void submit(FunctionType f)
    {
        work_queue.push(std::function<void()>(f));
    }
private:
    void worker_thread();
private:
    std::vector<std::thread> thread_pool;
    std::atomic_bool done;
    join_threads join;
    ThreadSafe_Queue<std::function<void()>> work_queue;
};

ServerThreadPool::ServerThreadPool()
: done(false), join(thread_pool)
{
    unsigned const thread_count=std::thread::hardware_concurrency();
    try
    {
        std::cout << thread_count << std::endl;
        for(unsigned i=0; i<thread_count; ++i)
        {
            thread_pool.push_back(std::thread(&ServerThreadPool::worker_thread,this));
        }
    }
    catch(...)
    {
        done=true;
        throw;
    }
}

void ServerThreadPool::worker_thread()
{
    while(!done)
    {
        std::function<void()> task;
        if(work_queue.try_pop(task))
        {
            task();
        }
        else
        {
            std::this_thread::yield();
        }
    }
}

std::mutex mut;
ThreadSafe_Queue<std::string> msg_queue;
std::vector<std::string> get_msg;

void getMsg()
{
    std::lock_guard<std::mutex> lock(mut);
    std::string msg;
    std::cout << "thread id : " << std::this_thread::get_id() << std::endl;
    if (msg_queue.try_pop(msg))
    {
        std::cout << msg << std::endl;
    }
}

int main()
{
    ServerThreadPool pool;
    msg_queue.push("work-1");
    msg_queue.push("work-2");   
    msg_queue.push("work-3");
    pool.submit(getMsg);
    pool.submit(getMsg);
    pool.submit(getMsg);
    pool.submit(getMsg);
    std::getchar();
    return 0;
}