#include <sys/socket.h>
#include <poll.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <limits.h>		/* for OPEN_MAX */
#include <strings.h>
#include <cstdlib>
#include <errno.h>
#include <stdio.h>
#include <iostream>

int
main(int argc, char **argv)
{
    int					i, maxi, listenfd, connfd, sockfd;
    int					nready;
    ssize_t				n;
    char				buf[100];
    socklen_t			clilen;
    struct pollfd		client[OPEN_MAX];
    struct sockaddr_in	cliaddr, servaddr;

    listenfd = socket(AF_INET, SOCK_STREAM, 0);

    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family      = AF_INET;
    servaddr.sin_addr.s_addr = inet_addr("0.0.0.0");
    servaddr.sin_port        = htons(8080);

    bind(listenfd, (struct sockaddr*) &servaddr, sizeof(servaddr));
    char *p = getenv("LISTENQ");
    listen(listenfd, 20);

    client[0].fd = listenfd;
    client[0].events = POLLRDNORM;
    for (i = 1; i < OPEN_MAX; i++)
        client[i].fd = -1;		/* -1 indicates available entry */
    maxi = 0;					/* max index into client[] array */
/* end fig01 */

/* include fig02 */
    for ( ; ; ) {
        nready = poll(client, maxi+1, -1);

        if (client[0].revents & POLLRDNORM) {	/* new client connection */
            clilen = sizeof(cliaddr);
            connfd = accept(listenfd, (struct sockaddr *) &cliaddr, &clilen);
            std::cout << "new client come" << std::endl;
            //printf("new client: %s\n", socket((struct sockaddr *) &cliaddr, clilen));
            for (i = 1; i < OPEN_MAX; i++)
                if (client[i].fd < 0) {
                    client[i].fd = connfd;	/* save descriptor */
                    break;
                }
            if (i == OPEN_MAX)
                exit(1);
                //err_quit("too many clients");

            client[i].events = POLLRDNORM;
            if (i > maxi)
                maxi = i;				/* max index in client[] array */

            if (--nready <= 0)
                continue;				/* no more readable descriptors */
        }

        for (i = 1; i <= maxi; i++) {	/* check all clients for data */
            if ( (sockfd = client[i].fd) < 0)
                continue;
            if (client[i].revents & (POLLRDNORM | POLLERR)) {
                if ( (n = read(sockfd, buf, 100)) < 0) {
                    if (errno == ECONNRESET) {
                        /*4connection reset by client */
                        std::cout << "client aborted connection\n" << std::endl;
#ifdef	NOTDEF
                        printf("client[%d] aborted connection\n", i);
#endif
                        close(sockfd);
                        client[i].fd = -1;
                    } else
                        exit(1);
                        //err_sys("read error");
                } else if (n == 0) {
                    /*4connection closed by client */
                    std::cout << "client close connection\n" << std::endl;
#ifdef	NOTDEF
                    printf("client[%d] closed connection\n", i);
#endif
                    close(sockfd);
                    client[i].fd = -1;
                } else
                    write(sockfd, buf, n);

                if (--nready <= 0)
                    break;				/* no more readable descriptors */
            }
        }
    }
}