#include <iostream>
#include <cstdlib>


 struct A
 {
     int *p;
     A(){}
     A(A&& a)
     {
         p = a.p;
         a.p = nullptr;
     }
     ~A ()
     {
         if (p)
            delete [] p;
     }
 };

int main()
{
    int i[2] = {1,2};
    for (int k : i)
    {
        k++;
    }
    for (int k : i)
    {
        std::cout << k << std::endl;
    }
    //A a1;
    //a1.p = new int[10];
    //A a2(std::move(a1));
    //if (a1.p == nullptr)
    //    std::cout << "move" << std::endl;
    // int *p = new int[10];
    // *(p+1) = 34;
    // int *d = nullptr;
    // d = std::move(p);
    // std::cout << *(d+1) << std::endl;
    // std::cout << *(p+1) << std::endl;

    //delete []p;
    //delete []d;
    // A a1, a2;
    // a1.p = nullptr;
    // a2.p = nullptr;

    // a1.p = new int[10];
    // *(a1.p) = 10;
    // a2 = std::move(a1);
    // std::cout << sizeof(a1.p) << std::endl;
    // if (a1.p == nullptr)
    // {
    //     std::cout << "move" << std::endl;
    // }
    // else
    // {

    //     std::cout << *(a1.p) << std::endl;
    //     delete [] a1.p;
    //     //std::cout << *(a1.p) << std::endl;
    // }

    // if (sizeof(a2.p) == 10)
    // {
    //     std::cout << "get" << std::endl;
    //     delete [] a2.p;
    // }
    // else if(a2.p != nullptr)
    // {
    //     std::cout << *(a2.p) << std::endl;
    // }


    return 0;
    //const char* env_p = std::getenv("PATH");
    //std::cout << "Your LISTENQ is: " << env_p << '\n';
    //return 0;
}
