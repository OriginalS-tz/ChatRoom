#include <ncurses.h>


int main() {
    int ch;
    /* 开始 curses 模式 */
    /* 禁用行缓冲 */
    /* 开启功能键响应模式 */
    /* 当执行 getch()函数的时候关闭键盘回显 */
    initscr();
    raw();
    keypad(stdscr, TRUE);
    noecho();
    printw("Type any character to see it in bold\n");
    ch = getch();
    /* 如果没有调用 raw()函数，必须按下 enter 键才可以执行下面的程序 */
    if(ch == KEY_F(1)) /* 如果没有调用 keypad()初始化，将不会执行这条语句 */ 
        printw("F1 Key pressed");
    /* 如果没有使用 noecho() 函数，一些难看的控制字符将会被打印到屏幕上 */
    else
    { 
        printw("The pressed key is ");
        //加粗
        attron(A_BOLD); 
        printw("you press %c", ch); 
        attroff(A_BOLD);
    } 
    refresh(); 
    getch(); 
    endwin(); 
    return 0;
}