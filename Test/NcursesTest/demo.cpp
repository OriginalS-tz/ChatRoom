#include <ncurses.h>
#include <vector>
#include <string>

struct WIN
{
    int win_h;
    int win_w;
    WINDOW *win;
    std::vector<std::string> buffer;
};

class ServerUI
{
public:
    ServerUI();
    void show();
    ~ServerUI();
private:
    void createStatus();
    void createHelp();
    void createInput();

    void initWin(const WIN& win);
    void getCommand();
private:
    int height;
    int width;

    WIN inputWIN;
    WIN statusWIN;
    WIN helpWIN;
};

void ServerUI::show()
{
    initWin(statusWIN);
    initWin(helpWIN);
    initWin(inputWIN);
    //echo();
    while(true)
    {
        getCommand();
    }
    //noecho();
}

void ServerUI::getCommand()
{
    char cmd[100];
    wmove(inputWIN.win, 1, 1);
    wgetstr(inputWIN.win, cmd);
    delwin(inputWIN.win);
    createInput();
    initWin(inputWIN);
}

ServerUI::ServerUI() 
{
    getmaxyx(stdscr, height, width);
    createStatus();
    createHelp();
    createInput();
}

void ServerUI::createStatus()
{
    statusWIN.win_h = height / 5 * 4;
    statusWIN.win_w = width / 3 * 2;
    statusWIN.win = newwin(statusWIN.win_h, statusWIN.win_w, 0, 0);
    statusWIN.buffer.emplace_back("this is status window");
}

void ServerUI::createHelp()
{
    helpWIN.win_h = height / 5 * 4;
    helpWIN.win_w = width / 3;
    helpWIN.win = newwin(helpWIN.win_h, width - statusWIN.win_w, 0, statusWIN.win_w);
    helpWIN.buffer.emplace_back("this is help window");
}

void ServerUI::createInput()
{
    inputWIN.win_h = height / 5;
    inputWIN.win_w = width;
    inputWIN.win = newwin(inputWIN.win_h, inputWIN.win_w, statusWIN.win_h, 0);
    //inputWIN.buffer.emplace_back("this is input window");
}

void ServerUI::initWin(const WIN& win)
{
    box(win.win, 0, 0);
    int row = 1;
    for (std::string data : win.buffer)
    {
        wmove(win.win, row, 1);
        row++;
        wprintw(win.win, data.c_str());
    }
    wrefresh(win.win);
}

ServerUI::~ServerUI()
{
    delwin(statusWIN.win);
    delwin(helpWIN.win);
    delwin(inputWIN.win);
}

int main()
{
    initscr();
    cbreak();
    //curs_set(0);
    refresh();
    ServerUI ui;
    ui.show();
    getch();
    endwin();
    return 0;
}