#include <iostream>

#include <sys/mman.h>   //for mmap munmap
#include <sys/stat.h>   //for stat fstat
#include <fcntl.h>      //for open
#include <unistd.h>     //for close
#include <dirent.h>
#include <string>
#include <sstream>


#define OPEN_FILE_FAILED -1
#define GET_FILE_INFO_FAILED -2
#define MAP_FILE_FAILED -3
#define MAP_FILE_OK 1

namespace FILE_TOOL
{
    std::string generateFileNameWithNO(const std::string& baseName)
    {
        DIR *dir;
        struct dirent *dirPtr;
        dir = opendir(".");
        int i = 0;
        std::string fileName;
        while((dirPtr = readdir(dir)))
        {
            fileName = dirPtr->d_name;
            if (fileName.find(baseName) != std::string::npos)
            {
                i++;
            }
        }
        fileName = baseName + "." + std::to_string(i);
        int fd = open(fileName.c_str(), O_CREAT | O_RDWR, 0666);
        write(fd, fileName.c_str(), fileName.size());
        close(fd);
        return fileName;
    }
}

class MFIO
{
public:
    MFIO() : filePointer(nullptr) {}

    int mountFile(const std::string &filePath)
    {
        fd = open(filePath.c_str(), O_RDWR);
        if (fd == -1)
            return OPEN_FILE_FAILED;

        if (fstat(fd, &fileInfo) == -1)
            return GET_FILE_INFO_FAILED;

        filePointer = (char *) mmap (NULL, fileInfo.st_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
        if (filePointer == MAP_FAILED)
        {
            filePointer = nullptr;
            return MAP_FILE_FAILED;
        }
        writeDataSize = fileInfo.st_size;
        return MAP_FILE_OK;
    }

    void saveFile()
    {
        ftruncate(fd, writeDataSize);
    }

    void umountFile()
    {
        saveFile();
        close(fd);
        munmap(filePointer, fileInfo.st_size);
        fd = -1;
        filePointer = nullptr;
    }

    bool hasMounted() const
    {
        return filePointer == nullptr? false : true;
    }

    void appendLine(std::string data)
    {
        data = "\n" + data;
        write(data);
    }

    template<typename T>
    MFIO& operator<<(T data)
    {
        std::string str = std::to_string(data);
        write(str);
        return *this;
    }

    MFIO& operator<<(const std::string &data)
    {
        write(data);
        return *this;
    }

    MFIO& operator<<(const char *data)
    {
        std::string str(data);
        write(str);
        return *this;
    }

    char* begin() const
    {
        return filePointer;
    }

    char* end() const
    {
        return filePointer + writeDataSize + 1;
    }

    std::string getStr() const
    {
        std::string data(filePointer);
        return data;
    }
private:

    void write(const std::string& data)
    {
        char *p = filePointer + writeDataSize;
        memcpy(p, data.c_str(), data.size());
        p += data.size();
        writeDataSize += data.size();
    }

    char *filePointer;
    int fd;
    int writeDataSize;
    struct stat fileInfo;
};

int main()
{
    std::string fileName = FILE_TOOL::generateFileNameWithNO("log");

    MFIO mfio;

    if (mfio.mountFile("log.1") != 1)
        return -1;

    std::stringstream i(mfio.getStr());
    std::string line;
    while(std::getline(i, line))
    {
        std::cout << line << std::endl;
    }
    std::cout << "map ok" << std::endl;
    line = "okok";
    mfio << line;
    //mfio.appendLine("hello,world");
    //mfio.appendLine("hello,world");
    mfio.umountFile();
    return 0;
}
