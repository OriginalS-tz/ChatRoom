#ifndef CXXLOG_HPP_
#define CXXLOG_HPP_
#include <stdlib.h>
#include <sys/msg.h>
#include <sys/time.h>
#include <unistd.h>

#include <cstdio>

#include <sys/mman.h>   //for mmap munmap
#include <sys/stat.h>   //for stat fstat
#include <fcntl.h>      //for open
#include <unistd.h>     //for close
#include <dirent.h>
#include <string>
#include <sstream>

#define OPEN_FILE_FAILED -1
#define GET_FILE_INFO_FAILED -2
#define MAP_FILE_FAILED -3
#define MAP_FILE_OK 1

namespace FILE_TOOL
{
    std::string generateFileNameWithNO(const std::string& baseName)
    {
        DIR *dir;
        struct dirent *dirPtr;
        dir = opendir(".");
        int i = 0;
        std::string fileName;
        while((dirPtr = readdir(dir)))
        {
            fileName = dirPtr->d_name;
            if (fileName.find(baseName) != std::string::npos)
            {
                i++;
            }
        }
        fileName = baseName + "." + std::to_string(i);
        int fd = open(fileName.c_str(), O_CREAT | O_RDWR, 0666);
        write(fd, fileName.c_str(), fileName.size());
        close(fd);
        return fileName;
    }
}

class MFIO
{
public:
    MFIO(const std::string &fileName) :
        baseName(fileName), filePointer(nullptr), writeDataSize(0) {}

    int mountFile(const std::string &filePath)
    {
        fd = open(filePath.c_str(), O_RDWR);
        if (fd == -1)
            return OPEN_FILE_FAILED;

        if (fstat(fd, &fileInfo) == -1)
            return GET_FILE_INFO_FAILED;

        filePointer = (char *) mmap (NULL, fileInfo.st_size + writeDataSize, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
        if (filePointer == MAP_FAILED)
        {
            filePointer = nullptr;
            return MAP_FILE_FAILED;
        }
        writeDataSize += fileInfo.st_size;
        return MAP_FILE_OK;
    }

    void saveFile()
    {
        if(mountFile(baseName))
        {
            memcpy(filePointer, buffer.c_str(), buffer.size());
            ftruncate(fd, writeDataSize);
            umountFile();
        }
    }

    void umountFile()
    {
        close(fd);
        munmap(filePointer, fileInfo.st_size);
        fd = -1;
        filePointer = nullptr;
    }

    bool hasMounted() const
    {
        return filePointer == nullptr? false : true;
    }

    void appendLine(std::string data)
    {
        data = "\n" + data;
        write(data);
    }

    template<typename T>
    MFIO& operator<<(T data)
    {
        std::string str = std::to_string(data);
        write(str);
        return *this;
    }

    MFIO& operator<<(const std::string &data)
    {
        write(data);
        return *this;
    }

    MFIO& operator<<(const char *data)
    {
        std::string str(data);
        write(str);
        return *this;
    }

    char* begin() const
    {
        return filePointer;
    }

    char* end() const
    {
        return filePointer + writeDataSize + 1;
    }

    std::string getStr() const
    {
        std::string data(filePointer);
        return data;
    }

    ~MFIO()
    {
        umountFile();
    }
private:

    void write(const std::string& data)
    {
        buffer += data;
        writeDataSize += data.size();
    }
    std::string baseName;
    std::string buffer;
    char *filePointer;
    int fd;
    int writeDataSize;
    struct stat fileInfo;
};

class FileWriter
{
public:
    static FileWriter* getInstance()
    {
        if (writer == nullptr)
        {
            std::string fileName = FILE_TOOL::generateFileNameWithNO("ServerLOG");
            instance = new MFIO(fileName);
            writer = new FileWriter();
        }
        return writer;
    }

    static MFIO* getMFIO()
    {
        return instance;
    }
    ~FileWriter()
    {
        delete instance;
        delete writer;
    }
private:
    FileWriter()
    {
    }
    static FileWriter *writer;
    static MFIO *instance;
};

FileWriter * FileWriter::writer = nullptr;
MFIO * FileWriter::instance = nullptr;

class END {};

class CXXLOG
{

public:
    CXXLOG(int line, const char * file, const char * function, const char * level, int color) : color(color)
    {
        if (writer == nullptr)
        {
            writer = FileWriter::getInstance();
        }

        msg += "\033[1;" + colors[color][0] + ";" + colors[color][1] + "m";
        char level_text[30];
        snprintf(level_text, 30, "[%-8s]", level);
#ifndef DISABLE_LEVEL
        msg += level_text;
#endif
        log += level_text;

        getTime();

        char file_text[30];
        snprintf(file_text, 30, "%-10s", file);
#ifndef DISABLE_FILE
        msg += file_text;
#endif
        log += file_text;

        char function_text[30];
        snprintf(function_text,30,"%10s:%-4d", function, line);
#ifndef DISABLE_FUNCTION
        msg += function_text;
#endif
        log+= function_text;
        log += " : ";
        msg += "\033[0m:\t";
    }

    CXXLOG& operator<<(END)
    {
        End();
        return *this;
    }

    CXXLOG& operator<<(char *str)
    {
        log += str;
        msg += str;
        return *this;
    }

    CXXLOG& operator<<(const char *str)
    {
        log += str;
        msg += str;
        return *this;
    }

    template<typename T>
    CXXLOG& operator<<(T value)
    {
        std::stringstream ss;
        ss << value;
        log += ss.str();
        msg += ss.str();
        return *this;
    }

    CXXLOG& stream()
    {
        return *this;
    }

private:
    void getTime()
    {
        struct timeval tv;
        struct timezone tz;
        struct tm *t;

        gettimeofday(&tv, &tz);
        t = localtime(&tv.tv_sec);

        char year[6];
        snprintf(year, 6, "%d-", t->tm_year+1900);

        char month[4];
        snprintf(month, 4, "%d-", t->tm_mon+1);

        char day[4];
        snprintf(day, 4, "%d-", t->tm_mday);

        char hour[4];
        snprintf(hour, 4, "%d-", t->tm_hour);

        char min[4];
        snprintf(min, 4, "%d-", t->tm_min);

        char sec[3];
        snprintf(sec, 3, "%d", t->tm_sec);
#ifndef DISABLE_TIME
        msg += year;
        msg += month;
        msg += day;
        msg += hour;
        msg += min;
        msg += sec;
#endif
        log += "[";
        log += year;
        log += month;
        log += day;
        log += hour;
        log += min;
        log += sec;
        log += "]";
    }

    void End()
    {
#ifdef NORMAL
        std::printf("%s\n", msg.c_str());
#endif
        auto i = writer->getMFIO();
        i->appendLine(log);
    }

private:
    std::string log;
    static FileWriter *writer;
    const int UNAVAILABLE = -1;
    std::string msg;
    int color;
    std::string colors[8][2] = {{"31","40"},{"37","40"},{"36","40"},{"33","40"},{"37","41"},{"37","45"},{"37","43"},{"37","44"}};
};

FileWriter* CXXLOG::writer = nullptr;

#define INFO __LINE__,__FILE__,__FUNCTION__
#define END END()

//level 1
#define LOG_DEBUG CXXLOG(INFO,"debug",0).stream()
#define LOG_INFO CXXLOG(INFO,"info",1).stream()
#define LOG_TRACE CXXLOG(INFO,"trace",2).stream()
#define LOG_WARN CXXLOG(INFO,"warm",3).stream()

//level 2
#define LOG_ERROR CXXLOG(INFO,"error",4).stream()
#define LOG_FATAL CXXLOG(INFO,"fatal",5).stream()
#define LOG_SYSERR CXXLOG(INFO,"syserr",6).stream()
#define LOG_SYSFATAL CXXLOG(INFO,"sysfatal",7).stream()
#endif
