#ifndef THREAD_POOL_IMPL_H_
#define THREAD_POOL_IMPL_H_

#include <vector>
#include <atomic>
#include "threadpool.h"
#include "../threadsafe_queue.hpp"

class ThreadPool::ThreadPool_Impl
{
  public:
    ThreadPool_Impl();
    ~ThreadPool_Impl();

    void submit(std::function<void()> f)
    {
        work_queue.push(f);
    }

  private:
    void worker_thread();

    std::vector<std::thread> thread_pool;
    std::atomic_bool done;
    ThreadSafe_Queue<std::function<void()>> work_queue;

    struct ThreadJoin
    {
        std::vector<std::thread> &threads;
        explicit ThreadJoin(std::vector<std::thread> &threads_) : threads(threads_) {}
        ~ThreadJoin()
        {
            for (unsigned long i = 0; i < threads.size(); ++i)
            {
                if (threads[i].joinable())
                {
                    threads[i].join();
                }
            }
        }
    };
    ThreadJoin join;
};
#endif