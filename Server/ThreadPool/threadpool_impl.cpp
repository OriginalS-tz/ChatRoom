#include "threadpool_impl.h"
#include <iostream>

ThreadPool::ThreadPool_Impl::ThreadPool_Impl()
    : done(false), join(thread_pool)
{
    unsigned const thread_count = std::thread::hardware_concurrency();
    try
    {
        for (unsigned i = 0; i < thread_count; ++i)
        {
            thread_pool.emplace_back(std::thread(&ThreadPool_Impl::worker_thread, this));
        }
    }
    catch (...)
    {
        done = true;
        throw;
    }
}

void ThreadPool::ThreadPool_Impl::worker_thread()
{
    while (!done)
    {
        std::function<void()> task;
        if (work_queue.try_pop(task))
        {
            task();
        }
        else
        {
            std::this_thread::yield();
        }
    }
}

ThreadPool::ThreadPool_Impl::~ThreadPool_Impl()
{
    done = true;
}
