#include "threadpool_impl.h"
#include "threadpool.h"
std::shared_ptr<ThreadPool> ThreadPool::pool;
//std::shared_ptr<ThreadPool::ThreadPool_Impl> ThreadPool::impl;
void ThreadPool::submit(std::function<void()> f)
{
    impl->submit(f);
}

ThreadPool::ThreadPool() : impl(std::make_shared<ThreadPool_Impl>())
{
}

std::shared_ptr<ThreadPool> ThreadPool::getThreadPool()
{
    if (!pool.get())
    {
        pool.reset(new ThreadPool);
    }
    return pool;
}