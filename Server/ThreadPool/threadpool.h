#ifndef THREAD_POOL_H
#define THREAD_POOL_H
#include <functional>
class ThreadPool
{
  public:
    static std::shared_ptr<ThreadPool> getThreadPool();
    
    // template <typename FunctionType>
    // void submit(FunctionType f)
    // {
    //     _submit(std::function<void()>(f));
    // }

    void submit(std::function<void()> f);

  private:
    ThreadPool(); 
    static std::shared_ptr<ThreadPool> pool;
    class ThreadPool_Impl;
    std::shared_ptr<ThreadPool_Impl> impl;
};
#endif