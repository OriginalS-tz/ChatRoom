#ifndef UI_H_
#define UI_H_
#include <string>
#include <memory>
class UI
{
public:
    UI();
    void show();
    void writeToStatus(const std::string& data);
    void writeToList(const std::string& data);
private:
    class UI_Impl;
    std::shared_ptr<UI_Impl> impl;
};
#endif