#ifndef UI_IMPL_H
#define UI_IMPL_H

#include <ncurses.h>
#include <vector>
#include <string>

#include "../Net/net.h"
#include "ui.h"
class UI::UI_Impl
{
  struct WIN
  {
    int win_h;
    int win_w;
    WINDOW *win;
  };

  struct runServer
  {
      Net* net;

      void operator()()
      {
          net = new Net();
          net->serve();
          delete net;
      }

      Net* getNet()
      {
          return net;
      }
  };
public:
  UI_Impl();
  ~UI_Impl();
  void show();
  void writeToStatus(const std::string &data);
  void writeToList(const std::string &data);
private:
  void changeWIN();
  void scrollWIN();

  void RefreshWIN();
  void getInput();

  void createStatus();
  void createList();
  void createInput();

  void initWin(const WIN &win);
  void getCommand();

  int height;
  int width;
  runServer runner;
  struct
  {
    std::vector<std::string> data = {"hello", "nice"};
    int begin_x, begin_y;
  }Buffer[2]; //0:status, 1:list

  int currentWIN;

  WIN inputWIN;
  WIN statusWIN;
  WIN listWIN;
  WIN* WINS[3];
};
#endif
