#include <thread>
#include <functional>
#include "ui_impl.h"

void UI::UI_Impl::show()
{
    std::thread server(runner);
    initWin(statusWIN);
    initWin(listWIN);
    initWin(inputWIN);
    curs_set(0);
    changeWIN();
    while (true)
    {
        getInput();
    }
}

void UI::UI_Impl::getCommand()
{
    while (true)
    {
        curs_set(0);
        keypad(inputWIN.win, true);
        {
            noecho();
            char key = getch();
            if (key == 9) //ESC
            {
                changeWIN();
                return;
            }
            else
            {
                curs_set(1);
                echo();
            }
        }
        char cmd[100];
        wmove(inputWIN.win, 1, 1);

        wgetstr(inputWIN.win, cmd);
        delwin(inputWIN.win);
        createInput();
        initWin(inputWIN);
    }
}

UI::UI_Impl::UI_Impl()
{
    initscr();
    cbreak();
    refresh();

    getmaxyx(stdscr, height, width);
    createStatus();
    createList();
    createInput();
    WINS[0] = &statusWIN;
    WINS[1] = &listWIN;
    WINS[2] = &inputWIN;
    currentWIN = 2;
}

void UI::UI_Impl::createStatus()
{
    statusWIN.win_h = height / 5 * 4;
    statusWIN.win_w = width / 3 * 2;

    Buffer[0].begin_x = statusWIN.win_w - 1;
    Buffer[0].begin_y = statusWIN.win_h - 1;

    statusWIN.win = newwin(statusWIN.win_h, statusWIN.win_w, 0, 0);
}

void UI::UI_Impl::createList()
{
    listWIN.win_h = height / 5 * 4;
    listWIN.win_w = width / 3;

    Buffer[1].begin_x = listWIN.win_w - 1;
    Buffer[1].begin_y = listWIN.win_h - 1;

    listWIN.win = newwin(listWIN.win_h, width - statusWIN.win_w, 0, statusWIN.win_w);
}

void UI::UI_Impl::createInput()
{
    inputWIN.win_h = height / 5;
    inputWIN.win_w = width;
    inputWIN.win = newwin(inputWIN.win_h, inputWIN.win_w, statusWIN.win_h, 0);
}

void UI::UI_Impl::initWin(const WIN &win)
{
    box(win.win, 0, 0);
    int row = 1;
    scrollok(win.win, 1);

    wrefresh(win.win);
}

UI::UI_Impl::~UI_Impl()
{
    delwin(statusWIN.win);
    delwin(listWIN.win);
    delwin(inputWIN.win);
    endwin();
}

void UI::UI_Impl::writeToList(const std::string &data)
{
    Buffer[1].data.push_back(data);
}

void UI::UI_Impl::writeToStatus(const std::string &data)
{
    Buffer[0].data.push_back(data);
}

void UI::UI_Impl::changeWIN()
{
    WINDOW *w = WINS[currentWIN]->win;
    box(w, 0, 0);

    wrefresh(w);
    currentWIN = (currentWIN + 1) % 3;
    w = WINS[currentWIN]->win;

    wmove(w, 0, 0);
    wprintw(w, "now");
    wrefresh(w);
}

void UI::UI_Impl::scrollWIN()
{
    // for (int i = 1; i < WINS[currentWIN]->win_h - 1; i++)
    // {
    //     wmove(WINS[currentWIN]->win, i,1);
    //     wprintw(WINS[currentWIN]->win, "hello");
    // }
}

void UI::UI_Impl::getInput()
{
    noecho();
    char key = getch();

    switch (key)
    {
    case 9:
        changeWIN();
        break;
    case '\n':
    {
        if (currentWIN == 2)
            getCommand();
        else
            scrollWIN();
        break;
    }
    default:
        break;
    }
    //echo();
}

void UI::UI_Impl::RefreshWIN()
{
}
