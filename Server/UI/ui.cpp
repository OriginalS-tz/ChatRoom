#include "ui.h"
#include "ui_impl.h"
UI::UI() : impl(std::make_shared<UI_Impl>())
{
}

void UI::show()
{
    impl->show();
}

void UI::writeToList(const std::string& data)
{
    impl->writeToList(data);
}

void UI::writeToStatus(const std::string& data)
{
    impl->writeToStatus(data);
}