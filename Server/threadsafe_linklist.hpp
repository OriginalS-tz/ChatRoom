#ifndef THREAD_SAFE_LINKLIST_H_
#define THREAD_SAFE_LINKLIST_H_
#include <thread>
#include <list>
template<typename T>
class ThreadSafe_LinkList
{
public:
    void push(const T &item)
    {
        std::lock_guard<std::mutex> lock(mut);
        list.push_back(item);
    }
private:
    mutable std::mutex mut;
    std::list<T> list;
};
#endif
