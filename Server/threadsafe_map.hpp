#ifndef THREADSAFE_MAP_HPP_
#define THREADSAFE_MAP_HPP_

#include <map>
#include <utility>
#include <thread>
//#include <vector>
//#include <iostream>

template<typename KEY, typename VALUE>
class ThreadSafe_Map
{
public:
    void add(KEY key, VALUE value)
    {
        //std::cout  << "add :" << key << "-" << value << std::endl;
        std::lock_guard<std::mutex> lock(mut);
        map[key] = value;
        //map.emplace(std::make_pair(key, value));
    }

    void remove(KEY key)
    {
        std::lock_guard<std::mutex> lock(mut);
        if (map.find(key) != map.end())
        {
            map.erase(map.find(key));
        }
    }

    bool getValue(KEY key, VALUE& value)
    {
        std::lock_guard<std::mutex> lock(mut);
        if (map.find(key) != map.end())
        {
            value = (map.find(key))->second;
            return true;
        }
        return false;
    }

    bool removeKeyByValue(const VALUE &value)
    {
        std::lock_guard<std::mutex> lock(mut);
        std::vector<KEY> key_list;
        //std::cout << "remove key, value=" << value << std::endl;
        for (auto i : map)
        {
            //std::cout << "find key :" << i.first << "-" << i.second << std::endl;
            if (i.second == value)
            {
                key_list.push_back(i.first);
            }
        }
        // for (auto beg = map.begin(); beg != map.end(); beg++)
        // {
        //     if (beg->second == value)
        //     {
        //         key_list.push_back(beg->first);
        //         //map.erase()
        //     }
        // }
        if (key_list.empty())
        {
            return false;
        }

        for (auto i : key_list)
        {
            map.erase(i);
        }
        return true;
    }

    bool has(KEY key)
    {
        std::lock_guard<std::mutex> lock(mut);
        return map.find(key) == map.end() ? false : true;
    }

    std::map<KEY, VALUE>
    getMap()
    {
        return map;
    }
private:
    mutable std::mutex mut;
    std::map<KEY, VALUE> map;
};
#endif
