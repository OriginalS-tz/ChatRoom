#include "ConnectionPool.h"
#include <iostream>

std::shared_ptr<ConnectionPool> ConnectionPool::instance;

ConnectionPool::ConnectionPool(const std::string &db)
    : CONNECTIONS(std::thread::hardware_concurrency()), done(false)
{
    for (int i = 0; i < CONNECTIONS; i++)
    {
        std::shared_ptr<Sqlite> s = std::make_shared<Sqlite>(db);
        if (s->isOpen())
        {
            //std::cout << "get a connection" << std::endl;
            pool.push(s); //转移连接的所有权
        }
        else
        {
            i--;
        }
    }
}

std::shared_ptr<ConnectionPool> 
ConnectionPool::getConnectionPool(const std::string &db)
{
    if (!instance.get())
    {
        instance.reset(new ConnectionPool(db));
    }
    return instance;
}

std::shared_ptr<Sqlite> 
ConnectionPool::getConnection()
{
    std::shared_ptr<Sqlite> connect;
    while (!done)
    {
        if (pool.try_pop(connect))
        {
            return connect;
        }
        else
        {
            std::this_thread::yield();
        }
    }
    return connect;
}


void ConnectionPool::releaseConnection(std::shared_ptr<Sqlite> connection)
{
    pool.push(connection);
}