#ifndef CONNECTIONPOOL_HPP_
#define CONNECTIONPOOL_HPP_
#include <thread>
#include "../threadsafe_queue.hpp"
#include "../SqliteInterface/sqlite.h"
//class Redis;

template <typename T>
class ConnectionPool
{
  public:
    std::shared_ptr<T> getConnection()
    {
        std::shared_ptr<Sqlite> connect;
        while (!done)
        {
            if (pool.try_pop(connect))
            {
                return connect;
            }
            else
            {
                std::this_thread::yield();
            }
        }
        return connect;
    }

    static std::shared_ptr<ConnectionPool<T>> getConnectionPool()
    {
        if (!instance.get())
        {
            instance.reset(new ConnectionPool());
        }
        return instance;
    }

    void releaseConnection(std::shared_ptr<T> connection)
    {
        pool.push(connection);
    }

    void setInit(bool set)
    {
        init = set;
    }

    ~ConnectionPool()
    {
        done = true;
    }

  private:
    ConnectionPool() : done(false){};

  private:
    static std::shared_ptr<ConnectionPool<T>> instance;
    static bool init;
    std::atomic_bool done;
    ThreadSafe_Queue<std::shared_ptr<T>> pool;
};

template <typename T>
bool ConnectionPool<T>::init = false;

template <typename T>
std::shared_ptr<ConnectionPool<T>>
    ConnectionPool<T>::instance;

namespace POOL
{
std::shared_ptr<ConnectionPool<Sqlite>>
GET_SQLITE_POOL(const std::string &db)
{
    auto sqlite_pool = ConnectionPool<Sqlite>::getConnectionPool();
    for (unsigned int i = 0; i < std::thread::hardware_concurrency(); i++)
    {
        std::shared_ptr<Sqlite> s = std::make_shared<Sqlite>(db);
        if (s->isOpen())
        {
            //std::cout << "get a connection" << std::endl;
            sqlite_pool->releaseConnection(s); //转移连接的所有权
        }
        else
        {
            i--;
        }
    }
    sqlite_pool->setInit(true);
    return sqlite_pool;
}

// std::shared_ptr<ConnectionPool<Redis>>
// GET_REDIS_POOL()
// {
//     auto sqlite_pool = ConnectionPool<Sqlite>::getConnectionPool();
//     for (int i = 0; i < std::thread::hardware_concurrency(); i++)
//     {
//         std::shared_ptr<Redis> s = std::make_shared<Redis>();
//         //todo:connect the redis
//     }
// }
} // namespace POOL
#endif
