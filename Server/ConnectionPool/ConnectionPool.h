#ifndef CONNECTIONPOOL_H_
#define CONNECTIONPOOL_H_
#include <thread>
#include "../SqliteInterface/sqlite.h"
#include "../threadsafe_queue.hpp"
#include <iostream>

class ConnectionPool
{
public:
    std::shared_ptr<Sqlite> getConnection();
    static std::shared_ptr<ConnectionPool> getConnectionPool(const std::string &db);
    void releaseConnection(std::shared_ptr<Sqlite> connection);
    ~ConnectionPool()
    {
        done = true;
    }
private:
    ConnectionPool(const std::string &db);

private:
    static std::shared_ptr<ConnectionPool> instance;
    const int CONNECTIONS;
    std::atomic_bool done;
    ThreadSafe_Queue<std::shared_ptr<Sqlite>> pool;
};

#endif