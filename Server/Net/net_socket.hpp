#ifndef NET_SOCKET_H
#define NET_SOCKET_H
#include <unistd.h>
#include <cerrno>
#include <poll.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdio.h>

namespace NET_SOCKET
{

void err_sys(const char *err)
{
    char buf[1024];
    puts(err);
    sprintf(buf + strlen(buf), ": %s", strerror(errno));
    //todo: put the error to log
}

int Socket(int family, int type, int protocol)
{
    int n;

    if ((n = socket(family, type, protocol)) < 0)
        err_sys("socket error");
    return (n);
}

int Accept(int fd, struct sockaddr *sa, socklen_t *salenptr)
{
    int n;

again:
    if ((n = accept(fd, sa, salenptr)) < 0)
    {
#ifdef EPROTO
        if (errno == EPROTO || errno == ECONNABORTED)
#else
        if (errno == ECONNABORTED)
#endif
            goto again;
        else
            err_sys("accept error");
    }
    return (n);
}

void Bind(int fd, const struct sockaddr *sa, socklen_t salen)
{
    if (bind(fd, sa, salen) < 0)
        err_sys("bind error");
}

void Connect(int fd, const struct sockaddr *sa, socklen_t salen)
{
    if (connect(fd, sa, salen) < 0)
        err_sys("connect error");
}

int Poll(struct pollfd *fdarray, unsigned long nfds, int timeout)
{
    int n;

    if ((n = poll(fdarray, nfds, timeout)) < 0)
        err_sys("poll error");

    return (n);
}

ssize_t Readn(int fd, char *buf, size_t n)
{
    size_t nleft;  //剩余数据量
    ssize_t nread; //已读数据量
    char *ptr;     //指向缓存区的指针
    ptr = buf;
    nleft = n;
    while (nleft > 0)
    {
        if ((nread = read(fd, ptr, nleft)) < 0)
        {
            if (errno == EINTR)
            {
                nread = 0;
            }
            else
            {
                return -1;
            }
        }
        else if (nread == 0)
        {
            break;
        }
        nleft -= nread;
        ptr += nread;
    }
    return (n - nleft);
}

ssize_t Writen(int fd, const char *buf, size_t n)
{
    size_t nleft;     //剩余数据
    ssize_t nwritten; //已写入数据，用于移动指针
    const char *ptr;  //指向buf的指针

    ptr = buf;
    nleft = n;
    while (nleft > 0)
    {
        if ((nwritten = write(fd, ptr, nleft)) <= 0)
        {
            if (nwritten < 0 && errno == EINTR)
            {
                nwritten = 0;
            }
            else
            {
                return -1;
            }
        }
        nleft -= nwritten;
        ptr += nwritten;
    }
    return n;
}

void Sig_chld(int)
{
    int stat;
    err_sys("error:");
    //while (waitpid(-1, &stat, WNOHANG) > 0)
    //    ;
}
} // namespace NET_SOCKET
#endif
