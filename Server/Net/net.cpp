#include "net.h"
#include "net_impl.h"

Net::Net()
: impl(std::make_shared<Net_Impl>())
{
}

void Net::serve()
{
	impl->serve();
}

bool Net::getClient(std::map<std::string, int> &get)
{
    return impl->getClients(get);
}

bool Net::getInfo(std::string &get)
{
    return impl->getInfo(get);
}
