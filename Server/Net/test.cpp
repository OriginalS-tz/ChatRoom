#include <string>
#include <iostream>
#include <regex>
int main()
{
    std::regex user_pattern("\\[(.+?)\\]");
    std::regex msg_pattern(":(.*)");
    std::string test = "[jam]to[Tom]:hello,world";
    std::smatch match;

    while (std::regex_search(test, match, user_pattern))
    {
        std::cout << match[1] << std::endl;
        test = match.suffix().str();
    }
    test = "[jam]to[Tom]:hello,world";
    if (std::regex_search(test, match, msg_pattern))
    {
        std::cout << match[1] << std::endl;
    }
    return 0;
}

