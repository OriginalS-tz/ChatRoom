#ifndef NET_IMPL_H_
#define NET_IMPL_H_

#include <arpa/inet.h>
#include "net.h"

#include "../ThreadPool/threadpool.h"
#include "../threadsafe_map.hpp"
#include "../ConnectionPool/ConnectionPool.h"

class Net::Net_Impl
{
public:
    Net_Impl ();

    ~Net_Impl ()
    {
        done = false;
    }

    void serve ();

    bool getClients (std::map<std::string, int> &get)
    {
        bool temp = isListChange;
        isListChange = false;
        get = userList.getMap();
        return temp;
    }

    bool getInfo (std::string &get)
    {
        return logs.try_pop(get);
    }

private:
    void initSocket ();

    void checkClient (int nready);

    int listenFD;
    std::shared_ptr<ThreadPool> threadPool;
    static std::shared_ptr<ConnectionPool> connectionPool;
    struct sockaddr_in clientAddress, serverAddress;
    static struct pollfd clientSocketForReceive[OPEN_MAX];
    static struct pollfd clientSocketForSend[OPEN_MAX];

    static int maxClients;

    static std::atomic_bool done;

    static ThreadSafe_Queue<std::string> buffer;
    static ThreadSafe_Queue<std::string> logs;

    static ThreadSafe_Map<std::string, int> userList;

    static std::atomic_bool isListChange;
    static ThreadSafe_Queue<std::pair<int, int>> taskDone;

    struct Task
    {
        int socketFD;
        int clientIndex;
    };

    struct Receive : Task
    {
        void operator() ();

        void userSignUp (const std::string &data);

        void userLogin (const std::string &userName);

        void userLoginCheck (const std::string &data);

        void privateChat (const std::string &data);

        void handleData (const std::string &data);

        void saveToDB (const std::string &data);

        void savePersonalHistory(std::string &user1, std::string &user2, std::string &msg);

        void savePublicHistory(std::string msg);

        bool receive (char *, ssize_t);

        void sendPersonalHistory(const std::string &data);

        void sendPublicHistory();
    };

    struct Send
    {
        void operator() ();
    };

    Send send;
};

#endif
