#include <regex>       //for regex
#include <signal.h>
#include "net_socket.hpp"
#include "net_impl.h"
#include "../Json/cxxjson.h"

#define DISABLE_LEVEL
#define DISABLE_TIME
#define DISABLE_FILE

#include "../cxxlog.hpp"

int Net::Net_Impl::maxClients;
struct pollfd Net::Net_Impl::clientSocketForReceive[OPEN_MAX];
struct pollfd Net::Net_Impl::clientSocketForSend[OPEN_MAX];

ThreadSafe_Map<std::string, int> Net::Net_Impl::userList;//user_name, socketFD
std::atomic_bool Net::Net_Impl::isListChange(false);

ThreadSafe_Queue<std::string> Net::Net_Impl::buffer;//store the message which send by clientSocketForReceive
ThreadSafe_Queue<std::string> Net::Net_Impl::logs;

ThreadSafe_Queue<std::pair<int, int>> Net::Net_Impl::taskDone;//store the clientSocketForReceive info(index, sock_fd) for reset the client[index].fd

std::atomic_bool Net::Net_Impl::done(false); //control the send thread
std::shared_ptr<ConnectionPool> Net::Net_Impl::connectionPool;

//=============================================================Tool Function
std::shared_ptr<char> generateDataPackage (const std::string &data)
{
    //generate a package : [data size][data]
    auto size = static_cast<unsigned int>(data.size());
    unsigned int sizeofData = htonl(size);

    LOG_TRACE << "Size of package:" << size << END;

    char *package = new char[5 + data.size()];

    bzero(package, 5 + size);
    memcpy(package, &sizeofData, 4);
    memcpy(package + 4, data.c_str(), data.size());
    std::shared_ptr<char> p(package, std::default_delete<char[]>());
    return p;
}

void formatString(std::string &str)
{
    str = "\"" + str + "\"";
}

std::pair<std::string, std::string>
parseData (const std::string &source)
{
    //match pattern : [data], get data
    std::string data[2];
    std::regex pattern("\\[(.+?)\\]");
    std::string temp = source;
    std::smatch match;

    int i = 0;
    while (std::regex_search(temp, match, pattern))
    {
        data[i++] = match[1];
        temp = match.suffix().str();
    }
    return std::make_pair(data[0], data[1]);
}
//=============================================================[END]Tool Function


Net::Net_Impl::Net_Impl ()
        : threadPool(ThreadPool::getThreadPool())
{
    //ignore the sigpipe signal, when you read / write a disconnect socket
    //you will receive -1 and errno will be set to EPIPE
    struct sigaction action;
    action.sa_handler = SIG_IGN;
    sigaction(SIGPIPE, &action, 0);

    //open the db and start the [Send] Thread
    connectionPool = ConnectionPool::getConnectionPool("server.db");
    initSocket();
    threadPool->submit(send);
}

void Net::Net_Impl::initSocket ()
{
    listenFD = NET_SOCKET::Socket(AF_INET, SOCK_STREAM, 0);
    bzero(&serverAddress, sizeof(serverAddress));

    //read the configure(address, port) from json
    std::string line, data;
    std::ifstream reader("configure.json");

    while (std::getline(reader, line))
    {
        data += line;
    }

    CXXJson jsonReader;
    jsonReader.loadJson(data);

    std::string address;
    int port;

    if (!jsonReader.getValue("address", address))
    {
        address = "0.0.0.0";
    }

    if (!jsonReader.getValue("port", port))
    {
        port = 10000;
    }

    //init the socket
    serverAddress.sin_family = AF_INET;
    serverAddress.sin_addr.s_addr = inet_addr(address.c_str());
    serverAddress.sin_port = htons(port);
}

void Net::Net_Impl::serve ()
{
    NET_SOCKET::Bind(listenFD, (struct sockaddr *) &serverAddress, sizeof(serverAddress));
    listen(listenFD, 5);

    //clientSocketForReceive[0].fd : socket for accept the connect require of clientSocketForReceive
    clientSocketForReceive[0].fd = listenFD;
    clientSocketForReceive[0].events = POLLIN;

    //prevent main thread from checking the socket which has been submit to sub thread
    clientSocketForSend[0].fd = -1;

    /*
     * clientSocketForReceive[1---OPEN_MAX-1]:
     * the socket of clientSocketForReceive which has connected to the server
     */
    for (int i = 1; i < OPEN_MAX; i++)
    {
        clientSocketForReceive[i].fd = -1;
        clientSocketForSend[i].fd = -1;
    }

    maxClients = 0;

    int nready;
    socklen_t cli_len;
    while (true)
    {
        nready = NET_SOCKET::Poll(clientSocketForReceive, maxClients + 1, 0);
        if (clientSocketForReceive[0].revents & POLLIN)
        {
            LOG_INFO << "\n\n+++++++++New Client Come+++++++++" << END;
            cli_len = sizeof(clientAddress);
            int client_fd = NET_SOCKET::Accept(listenFD, (struct sockaddr *) &clientAddress, &cli_len);

            int find;

            //check the server socket , for accepting the clientSocketForReceive
            //send the clientSocketForReceive socket for send & receive
            for (find = 1; find < OPEN_MAX; find++)
            {
                //find a clientSocketForReceive for send
                if (clientSocketForReceive[find].fd < 0)
                {
                    clientSocketForReceive[find].fd = client_fd;
                    clientSocketForSend[find].fd = client_fd;
                    break;
                }
            }
            if (find == OPEN_MAX)
            {
                //logs.push("too many clients");
                NET_SOCKET::err_sys("too many clients");
            }

            //sent the event : receive all message
            clientSocketForReceive[find].events = POLLIN;
            if (find > maxClients)
            {
                maxClients = find;
            }

            if (--nready <= 0)
            {
                continue;
            }
        }
        checkClient(nready);
    }
}

void Net::Net_Impl::checkClient (int nready)
{
    //check the exist connection for message
    int socketFd;
    std::pair<int, int> task;
    while (!taskDone.empty())
    {
        taskDone.try_pop(task);
        clientSocketForReceive[task.first].fd = task.second;
    }

    for (int i = 1; i <= maxClients; i++)
    {
        if ((socketFd = clientSocketForReceive[i].fd) < 0)
        {
            continue;
        }
        //check the socket weather the message has arrived
        if (clientSocketForReceive[i].revents & (POLLIN | POLLERR))
        {
            Receive receiveTask;
            receiveTask.socketFD = socketFd;
            receiveTask.clientIndex = i;

            clientSocketForReceive[i].fd = -1;
            std::string info = "Get a Message from : ";
            logs.push(info + inet_ntoa(clientAddress.sin_addr));

            isListChange = true;
            LOG_INFO << "Start a THREAD for Client" << END;
            threadPool->submit(receiveTask);
            if (--nready <= 0)
            {
                break;
            }
        }
    }
}

void Net::Net_Impl::Receive::operator() ()
{
    //data format:[data size][data]
    //data size : 4 byte
    //data : data_size byte
    char buf[10240];
    int size = 0;
    char size_byte[4];
    LOG_TRACE << "Receive From :" << socketFD << END;
    bzero(buf, 10240);
    bzero(size_byte, 4);

    //receive the data size
    if (receive(size_byte, 4))
    {
        memcpy(&size, size_byte, 4);
        size = ntohl(size); //net to host
        LOG_TRACE << "Size of DATA : " << size << END;
        //std::cout << size << std::endl;
        //the clientSocketForReceive will sent the heart beat package : 0xffff to check the link
        if (size > 0)
        {
            if (receive(buf, size))
            {
                LOG_TRACE << "GET Message" << buf << END;
                std::string data(buf, size);
                handleData(data);
            }
        }
        else
        {
            LOG_ERROR << "The size of package : " << size << END;
        }
    }
    taskDone.push(std::make_pair(clientIndex, socketFD));
}

bool Net::Net_Impl::Receive::receive (char *buf, ssize_t size)
{
    ssize_t n;
    if ((n = NET_SOCKET::Readn(socketFD, buf, size)) < 0)
    {
        if (errno == ECONNRESET || errno == EPIPE)
        {
            LOG_WARN << "the client aborted the connection" << END;
        }
        else
        {
            NET_SOCKET::err_sys("read error");
        }
    }
    else if (n == 0)
    {
        //logs.push("clientSocketForReceive close the connection");
        LOG_WARN << "client close the connection" << END;
    }
    else
    {
        LOG_TRACE << "Receive Data from client[" << clientIndex << "]" << ":" << socketFD << END;
        LOG_TRACE << "Expect Data SIZE :" << size << END;
        LOG_TRACE << "Receive Data SIZE :" << n << END;
        LOG_TRACE << "Data :" << buf << END;
        return true;
    }

    close(socketFD);
    if (userList.removeKeyByValue(socketFD))
    {
        isListChange = true;
        LOG_TRACE << "remove the client from user list" << END;
    }
    else
    {
        LOG_ERROR << "Cant remove the user form user list : NO SUCH A USER" << END;
    }
    socketFD = -1;
    return false;
}

static std::map<std::string, int> RequireMap =
        {
                {"1", 1}, //User Login
                {"2", 2}, //Private Chat
                {"3", 3}, //Public Chat
                {"4", 4}, //Exit
                {"5", 5}, //SignUp
                {"6", 6}, //Login Check
                {"7", 7}, //File Name
                {"8", 8}, //File Data
                {"A", 9}, //Get Public History
                {"B", 10} //Get Private History
        };

void Net::Net_Impl::Receive::handleData (const std::string &data)
{
    size_t pos = data.find_first_of('#');
    LOG_INFO << "Receive data " << END;
    if (pos != std::string::npos)
    {
        std::string require = data.substr(0, pos);
        std::string user_data = data.substr(pos + 1);
        LOG_TRACE << "require :" << require << END;
        auto i = RequireMap.find(require);
        if (i != RequireMap.end())
        {
            switch (i->second)
            {
                case 1:
                    userLogin(user_data);
                    break;
                case 2:
                    privateChat(data);
                    break;
                case 5:
                    userSignUp(data);
                    break;
                case 6:
                    userLoginCheck(data);
                    break;
                case 9:
                    sendPublicHistory();
                    break;
                case 10:
                    sendPersonalHistory(data);
                    break;
                case 3:
                    {
                        std::string msg = data.substr(pos + 1);
                        savePublicHistory(msg);
                    }
                default:
                    buffer.push(data);
                    //saveToDB(data);
            }
        }
    }
}

void Net::Net_Impl::Receive::privateChat (const std::string &data)
{
    //use regex : format : [user1]to[user2]:message
    //user_name[0] : sender
    //user_name[1] : receiver
    std::string sender;
    std::string receiver;
    int receiverSocket;


    std::smatch match;

    auto names = parseData(data);
    sender = names.first;
    receiver = names.second;
    LOG_INFO << "Private Chat" << sender << " TO " << receiver << END;

    std::string msg = data.substr(data.find(':'));

    if (userList.getValue(receiver, receiverSocket))
    {
        std::string msg_send = "2#[" + sender + "]:" + msg;
        auto package = generateDataPackage(msg_send);
        if (NET_SOCKET::Writen(receiverSocket, package.get(), msg_send.size() + 4) > 0)
        {
            LOG_INFO << "Private Chat : Send Successfully" << END;
            savePersonalHistory(sender, receiver, msg);
        }
        else
        {
            LOG_WARN << "Private Chat" << "Client has disconnected, Remove" << END;
            if (userList.removeKeyByValue(receiverSocket))
            {
                LOG_TRACE << "REMOVE Successfully" << END;
            }
            else
            {
                LOG_ERROR << "REMOVE FAIL" << END;
            }
        }
    }
}

void Net::Net_Impl::Receive::userLoginCheck (const std::string &data)
{
    std::string userName;
    std::string userPassword;

    auto userData = parseData(data);
    userName = userData.first;
    userPassword = userData.second;

    auto sqlite = connectionPool->getConnection();
    std::string sql = "select * from user where user_name='" + userName
                      + "' and user_pw='" + userPassword + "'";
    LOG_INFO << "command:" << sql << END;
    ResultSet queryResult = sqlite->query(sql);
    std::shared_ptr<char> package;

    std::string result;
    if (queryResult.empty())
    {
        LOG_WARN << "Login FAIL" << END;
        result = "6#false";
    }
    else
    {
        LOG_TRACE << "login SUCCESSFULLY" << END;
        result = "6#true";
    }
    package = generateDataPackage(result);
    if (NET_SOCKET::Writen(socketFD, package.get(), result.size() + 4))
    {
        LOG_TRACE << "Send Login Result SUCCESSFULLY" << END;
    }
    connectionPool->releaseConnection(sqlite);
}

void Net::Net_Impl::Receive::userLogin (const std::string &userName)
{
    //send the user list to clientSocketForReceive
    //send 1#user_name to other clients, notify other clients
    LOG_INFO << "User Login :" << userName << END;
    userList.add(userName, socketFD);
    auto allUsers = userList.getMap();

    std::string userListForSend = "0#";
    if (!allUsers.empty())
    {
        for (auto user : allUsers)
        {
            if (user.first != userName)
            {
                userListForSend += user.first + ",";
            }
        }

        if (userListForSend.size() > 2)
            userListForSend.resize(userListForSend.size() - 1);

        auto data = generateDataPackage(userListForSend);
        LOG_TRACE << "Send USER_LIST to " << userName << ":::---" << socketFD << "::" << userListForSend << END;
        //when the clientSocketForReceive disconnect, it will tigger off a error
        //so, remove the socket
        if (NET_SOCKET::Writen(socketFD, data.get(), userListForSend.size() + 4) <= 0)
        {
            if (userList.removeKeyByValue(socketFD))
            {
                LOG_TRACE << "remove successfully" << END;
            }
            socketFD = -1;
        }
        else
        {
            buffer.push("1#" + userName);
        }
    }
}

void Net::Net_Impl::Receive::userSignUp (const std::string &data)
{
    //if sign up successfully
    //  send 5#true to clientSocketForReceive
    //else
    //  send 5#false
    std::string userName;
    std::string userPassword;

    auto userData = parseData(data);
    userName = userData.first;
    userPassword = userData.second;

    auto sqlite = connectionPool->getConnection();
    std::string sql = "select * from user where user_name='" + userName + "'";

    ResultSet queryResult = sqlite->query(sql);

    std::shared_ptr<char> package;
    std::string result;

    if (!queryResult.empty())
    {
        result = "5#false";
    }
    else
    {
        PaperValues userInfo;
        userInfo["user_name"] = "'" + userName + "'";
        userInfo["user_pw"] = "'" + userPassword + "'";

        if (sqlite->insert("user", userInfo))
            result = "5#true";
        else
            result = "5#false";
    }
    package = generateDataPackage(result);

    connectionPool->releaseConnection(sqlite);
    NET_SOCKET::Writen(socketFD, package.get(), result.size() + 4);
}

void Net::Net_Impl::Receive::sendPersonalHistory(const std::string &data)
{
    auto sql = connectionPool->getConnection();
    auto users = parseData(data);
    std::string user1 = users.first;
    std::string user2 = users.second;

    formatString(user1);
    formatString(user2);

    ResultSet result[2];
    std::string IDs[2];

    LOG_INFO << user1 << END;
    LOG_INFO << user2 << END;

    result[0] = sql->query("select id from user where user_name=" + user1);
    result[1] = sql->query("select id from user where user_name=" + user2);

    for (int i = 0; i < 2; i++)
    {
        if (!result[i].empty())
        {
            auto map = result[i].back();
            IDs[i] = map["id"];
        }
    }

    std::string
    cmd = "select msg_id from personal_history where user_id="+ IDs[0]
        + " intersect "
        + "select msg_id from personal_history where user_id=" + IDs[1];
    LOG_INFO << cmd << END;
    ResultSet msgIDs = sql->query(cmd);

    std::stringstream msgs;
    msgs << "B#";
    for (auto map : msgIDs)
    {
        ResultSet m = sql->query("select msg from personal_msg where id="+map["msg_id"]);
        auto mmap = m.back();
        msgs << mmap["msg"] << "\n";
    }
    std::string personalMsg = msgs.str();
    LOG_INFO << "Personal History:"  << personalMsg << END;
    auto package = generateDataPackage(personalMsg);
    connectionPool->releaseConnection(sql);
    NET_SOCKET::Writen(socketFD, package.get(), personalMsg.size() + 4);
}

void Net::Net_Impl::Receive::sendPublicHistory()
{
    auto sql = connectionPool->getConnection();
    auto result = sql->query("select * from msg");
    std::stringstream history;
    history << "A#";
    if (!result.empty())
    {
        for (auto map : result)
        {
            history << map["data"] << "\n";
        }
        std::string msgHistory = history.str();
        connectionPool->releaseConnection(sql);
        auto package = generateDataPackage(msgHistory);
        LOG_WARN << "Public History :" << msgHistory << END;
        NET_SOCKET::Writen(socketFD, package.get(), msgHistory.size() + 4);
    }
}

void Net::Net_Impl::Receive::savePersonalHistory(std::string &user1, std::string &user2, std::string &msg)
{
    PaperValues value;
    msg = user1 + ":" + msg;
    formatString(user1);
    formatString(user2);
    formatString(msg);

    auto sql = connectionPool->getConnection();

    ResultSet result[2];
    std::string IDs[2];

    result[0] = sql->query("select id from user where user_name = " + user1);
    result[1] = sql->query("select id from user where user_name = " + user2);

    for (int i = 0; i < 2; i++)
    {
        if (!result[i].empty())
        {
            auto map = result[i].back();
            IDs[i] = map["id"];
        }
    }

    PaperValues paperValues;
    paperValues["msg"] = msg;
    sql->insert("personal_msg", paperValues);

    auto msgIDResult = sql->query("select id from personal_msg where msg=" + msg);
    if (!msgIDResult.empty())
    {
        auto map = msgIDResult.back();
        for (int i = 0; i < 2; i++)
        {
            sql->insert("personal_history", {{"user_id",IDs[i]}, {"msg_id", map["id"]}});
        }
    }
    connectionPool->releaseConnection(sql);
}

void Net::Net_Impl::Receive::savePublicHistory(std::string msg)
{
    auto sql = connectionPool->getConnection();
    PaperValues value;
    formatString(msg);
    value["data"] = std::string(msg);
    sql->insert("msg", value);
    connectionPool->releaseConnection(sql);
}

void Net::Net_Impl::Send::operator() ()
{
    std::shared_ptr<char> data;
    while (!done)
    {
        std::string get;
        if (buffer.try_pop(get))
        {
            data = generateDataPackage(get);
            for (int i = 1; i < OPEN_MAX; i++)
            {
                if (clientSocketForSend[i].fd == -1)
                    continue;
                if (NET_SOCKET::Writen(clientSocketForSend[i].fd, data.get(), get.size() + 4) <= 0)
                {
                    LOG_WARN << "SEND : close the connection of " << clientSocketForSend[i].fd << END;
                    close(clientSocketForSend[i].fd);
                    userList.removeKeyByValue(clientSocketForSend[i].fd);
                    clientSocketForSend[i].fd = -1;
                    LOG_ERROR << "SEND ERROR" << END;
                }
                else
                {
                    LOG_TRACE << "SEND to " << clientSocketForSend[i].fd << "\n\n" << END;
                }
            }
            LOG_TRACE << "FINISH SEND" << END;
        }
        else
        {
            std::this_thread::yield();
        }
    }
}
