#ifndef NET_H_
#define NET_H_
#include <memory>
#include <map>
//#include <arpa/inet.h>
class Net
{
public:
  Net();
  bool getClient(std::map<std::string, int> &get);
  bool getInfo(std::string &get);
  void serve();
private:
  class Net_Impl;
  std::shared_ptr<Net_Impl> impl;
};
#endif
