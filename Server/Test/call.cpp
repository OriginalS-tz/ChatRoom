#include <functional>
#include <iostream>
#include <string>

struct mycall
{
    int i = 0;
    std::string k = "hello";
    void operator()()
    {
        std::cout << i << std::endl;
        std::cout << k << std::endl;
    }

    void log()
    {
        std::cout << "this is a log" << std::endl;
    }
};

void callFunc(std::function<void()> f)
{
    f();
}

int main()
{
    mycall k;
    k.i = 2;
    callFunc(k);
    return 0;
}