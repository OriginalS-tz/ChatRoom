#include <iostream>
#include <thread>
#include "../ThreadPool/threadpool.h"
std::mutex mut;

void worker()
{
    std::lock_guard<std::mutex> lock(mut);
    std::cout << "this thread is " << std::this_thread::get_id() << std::endl;
    std::cout << "hello,world" << std::endl;
}


int main()
{
    auto pool = ThreadPool::getThreadPool();
    pool->submit(worker);
    pool->submit(worker);
    pool->submit(worker);
    return 0;
}