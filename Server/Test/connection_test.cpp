#include "../ConnectionPool/ConnectionPool.h"
#include <iostream>
#include <vector>

//std::mutex mut;
std::shared_ptr<ConnectionPool> connection_pool;
void func()
{
    std::shared_ptr<Sqlite> db = connection_pool->getConnection();
    ResultSet result = db->query("select * from user");
    connection_pool->releaseConnection(db);
    
    //std::lock_guard<std::mutex> look(mut);
    //std::cout << "this thread is " << std::this_thread::get_id() << std::endl;
    for (auto i : result)
    {
        auto begin = i.begin();
        while (begin != i.end())
        {
            std::cout << begin->first << " : " << begin->second << std::endl;
            begin++;
        }
    }
}

Sqlite *p;
std::vector<Sqlite *> v;
void get()
{
    auto db = std::move(v.back());
    v.pop_back();
   ResultSet result = db->query("select * from user");
    for (auto i : result)
    {
        auto begin = i.begin();
        while (begin != i.end())
        {
            std::cout << begin->first << " : " << begin->second << std::endl;
            begin++;
        }
    } 
}
int main()
{
    connection_pool = ConnectionPool::getConnectionPool("test.db");
    //auto pool = ThreadPool::getThreadPool();
    func();
    //auto f = func;
    //pool->submit(func);
    //pool->submit(func);
    //func();
    //Sqlite *connect = new Sqlite();
    //int *i = new int[23];
    //p = new Sqlite("test.db");
    //v.push_back(p);
    //get();
    return 0;
}