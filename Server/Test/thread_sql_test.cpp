//#include "../ConnectionPool/ConnectionPool.h"
#include "../ConnectionPool/connection_pool.hpp"
//#include "../ThreadPool/threadpool.h"
#include <iostream>
#include <vector>

//std::mutex mut;
std::shared_ptr<ConnectionPool<Sqlite>> connection_pool;
void func()
{
    std::shared_ptr<Sqlite> db = connection_pool->getConnection();
    ResultSet result = db->query("select * from user");
    connection_pool->releaseConnection(db);
    
//    std::lock_guard<std::mutex> look(mut);
//    std::cout << "this thread is " << std::this_thread::get_id() << std::endl;
    for (auto i : result)
    {
        auto begin = i.begin();
        while (begin != i.end())
        {
            std::cout << begin->first << " : " << begin->second << std::endl;
            begin++;
        }
    }
}

int main()
{
    //connectionPool = ConnectionPool::getConnectionPool("test.db");
    connection_pool = POOL::GET_SQLITE_POOL("test.db");
    //auto pool = ThreadPool::getThreadPool();
    //pool->submit(func);
    //pool->submit(func);
    func();
    std::getchar();
    return 0;
}

