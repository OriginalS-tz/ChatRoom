#include <iostream>
#include "../SqliteInterface/sqlite.h"
//#include "sqlitecom.h"
int main()
{
    //Sqlite *db = Sqlite::getInstance();
    //db->open("test.db");
    //if (db->isOpen())
    Sqlite db;
    db.open("test.db");
    {
        ResultSet result = db.query("select * from user");
        for (auto i : result) {
        auto begin = i.begin();
            while (begin != i.end()) {
                std::cout << begin->first << " : " << begin->second << std::endl; 
                begin++;
            }
        }
    }
    return 0;
}