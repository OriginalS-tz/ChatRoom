#include <fstream>
#include <iostream>

#include "../Json/cxxjson.h"
int main()
{
    CXXJson j;
    std::string data;
    std::string line;
    std::ifstream reader("test.json");
    while (std::getline(reader, line))
    {
        data += line;
    }
    j.loadJson(data);

    std::string value;
    if (j.getValue("word", value))
    {
        std::cout << value << std::endl;
    }

    if (j.setValue("word", 9999999))
    {
        j.saveJsonFile("nice.json");
    }

    if (j.setValue("word", "hello,world"))
    {
        j.saveJsonFile("nice2.json");
    }

    CXXJson node = j.getNodeTree("hard");

    node.setValue("name", 9999);

    if (j.setValue("word", node.getNode()))
    {
        j.saveJsonFile("nice3.json");
        std::cout << "ok" << std::endl;
    }

    CXXJson newTree(node);
    newTree.saveJsonFile("nice4.json");
    return 0;
}