#ifndef SQLITE_H_
#define SQLITE_H_

#include <map>
#include <string>
#include <vector>

#include <sqlite3.h>

typedef std::map<std::string, std::string> PaperValues;
typedef std::vector<PaperValues> ResultSet;

class Sqlite
{
public:
  Sqlite() : db(nullptr){};
  Sqlite(std::string dbName) : db(nullptr)
  {
    open(dbName);
  }
  bool isOpen();
  ResultSet query(const std::string &command);
  bool open(const std::string &dbName);
  bool insert(const std::string &table, PaperValues values);
  bool del(const std::string &command);
  bool update(const std::string &table, PaperValues values, const std::string &condition);

  ~Sqlite();

private:
  bool exec(const std::string &command);

private:
  sqlite3 *db;
};
#endif