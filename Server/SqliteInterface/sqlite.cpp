#include "sqlite.h"

bool Sqlite::exec(const std::string &command)
{
    char *cErrMsg;
    int nRes = sqlite3_exec(db, command.c_str(), 0, 0, &cErrMsg);
    if (nRes != SQLITE_OK)
        perror(cErrMsg);
    return nRes == SQLITE_OK ? true : false;
}

bool Sqlite::del(const std::string &command)
{
    return exec(command);
}
ResultSet Sqlite::query(const std::string &command)
{
    char *ErrSmg;
    char **pResult;
    int nRow;
    int nCol;
    int nResult = sqlite3_get_table(db, command.c_str(), &pResult, &nRow, &nCol, &ErrSmg);
    ResultSet result;
    if (nResult != SQLITE_OK)
    {
        perror(ErrSmg);
        return result;
    }
    else
    {
        int nIndex = nCol;
        for (int i = 0; i < nRow; i++)
        {
            PaperValues p;
            for (int j = 0; j < nCol; j++)
            {
                p[pResult[j]] = pResult[nIndex];
                ++nIndex;
            }
            result.emplace_back(p);
        }
    }
    return result;
}

bool Sqlite::update(const std::string &table, PaperValues values, const std::string &condition)
{
    std::string command = "update :0 set :1 where :2";
    auto begin = values.begin();
    auto end = values.end();

    std::string updateValues = "";
    auto count = values.size() - 1;

    while (begin != end)
    {
        updateValues += begin->first;
        updateValues += "=";
        updateValues += begin->second;
        if (count)
        {
            updateValues += ",";
        }
        count--;
        begin++;
    }
    command.replace(command.find(":0"), 2, table);
    command.replace(command.find(":1"), 2, updateValues);
    command.replace(command.find(":2"), 2, condition);
    return exec(command);
}

bool Sqlite::insert(const std::string &table, PaperValues values)
{
    std::string command = "insert into :0(:1) values(:2)";
    auto begin = values.begin();
    auto end = values.end();

    std::string tableName = "";
    std::string tableValues = "";
    auto count = values.size() - 1;
    while (begin != end)
    {
        tableName += begin->first;
        tableValues += begin->second;
        if (count)
        {
            tableName += ",";
            tableValues += ",";
        }
        count--;
        begin++;
    }
    command.replace(command.find(":0"), 2, table);
    command.replace(command.find(":1"), 2, tableName);
    command.replace(command.find(":2"), 2, tableValues);
    //puts(command.c_str());
    return exec(command);
}

bool Sqlite::open(const std::string &dbName)
{
    int open;

    if (db)
        sqlite3_close(db);

    open = sqlite3_open(dbName.c_str(), &db);
    if (open)
    {
        perror("cant open db");
        return false;
    }
    else
    {
        return true;
    }
}

bool Sqlite::isOpen()
{
    return db == nullptr ? false : true;
}

Sqlite::~Sqlite()
{
    if (db)
    {
        sqlite3_close(db);
        //perror("close link");
    }
    //perror("delete the obj");
}
