#include "cxxjson.h"
#include "iostream"
#include <sstream>
#include <fstream>

// #define DISABLE_LINE
// #define DISABLE_TIME
// #define DISABLE_FILE
// #define DISABLE_FUNCTION
// #define NORMAL
// #include "cxxlog.hpp"
// #define LOG

void CXXJsonParser::skip(str_iter &iter)
{
    while (*iter == ' ' || *iter == '\t' || *iter == '\n')
    {
        iter++;
    }
}

void CXXJsonParser::parse_error(str_iter &iter, char c)
{
    skip(iter);
    if (*iter != c)
    {
        std::cout << "--parse error:except " << c << " ,match " << *iter << std::endl;
        exit(0);
    }
}

std::string CXXJsonParser::match(str_iter &iter, char end)
{
    std::stringstream ss;
    skip(iter);
    while (*iter != end)
    {
        ss << *iter;
        iter++;
    }
    return ss.str();
}

std::string CXXJsonParser::matchNumber(str_iter &iter)
{
    std::stringstream ss;
    skip(iter);
    while (isdigit(*iter))
    {
        ss << *iter;
        iter++;
    }
    return ss.str();
}

JNode CXXJsonParser::parse_value(str_iter &iter)
{
    JNode jnode;
    JValue v;
    skip(iter);
    switch (*iter)
    {
    case '\"':
    {
        iter++;
        std::string value = match(iter, '\"');
        iter++;
#ifdef LOG
        LOG_TRACE << value << END;
#endif
        v.setValue(value);
        jnode.setValue(value);
        break;
    }
    case '{':
    {
#ifdef LOG
        LOG_WARN << "parse a sub obj" << END;
#endif
        jnode = parse(iter);
        iter++;
        jnode.setRoot();
        break;
    }
    case '[':
    {
#ifdef LOG
        LOG_WARN << "parse a array" << END;
#endif
        iter++;
        std::string value = match(iter, ']');
        iter++;
        jnode.setValue(value);
#ifdef LOG
        LOG_TRACE << value << END;
#endif
        break;
    }
    default:
        std::string value = matchNumber(iter);
#ifdef LOG
        LOG_TRACE << value << ":" << END;
#else
        //std::cout << atoi(value.c_str()) << std::endl;
#endif
        v.setValue(atoi(value.c_str()));
        jnode.setValue(value);
    }
    skip(iter);
    if (*iter != ',' && *iter != '}')
    {
#ifdef LOG
        LOG_DEBUG << "--parse error:except , or }, match " << *iter << END;
#else
        std::cout << "--parse error:except , or }, match " << *iter << std::endl;
#endif
        exit(0);
    }
    jnode.v = v;
    // if (v.isNumber())
    // {
    //     std::cout << "this is a int : " << v.getNumber() << std::endl;
    // }
    // else if (v.isString())
    // {
    //     std::cout << "this is a string : " << v.getString() << std::endl;
    // }
    return jnode;
}

JNode CXXJsonParser::parse_key(str_iter &iter)
{
    skip(iter);
    if (*iter == '}')
    {
        JNode j;
        j.isEmpty = true;
        return j;
    }
    if (*iter != '\"')
    {
        parse_error(iter, '\"');
    }
    iter++;
    std::string key = match(iter, '\"');
    iter++;
    if (*iter != ':')
    {
        parse_error(iter, ':');
    }
    //point to value
    iter++;
#ifdef LOG
    LOG_TRACE << "key  :  " << key << END;
#endif
    JNode k;
    k.setKey(key);
    return k;
}

JData CXXJsonParser::parse_data(str_iter &iter)
{
    JNode key_node = parse_key(iter);
    if (key_node.isEmpty)
    {
        return std::make_pair(key_node, key_node);
    }
    JNode value_node = parse_value(iter);
    JData jdata = std::make_pair(key_node, value_node);
    skip(iter);
    if (*iter == ',')
    {
        iter++;
    }
    else if (*iter != '}')
        iter++; //match " / }
    return jdata;
}

JNode CXXJsonParser::parse(str_iter &iter)
{
    JNode node;
    node.setRoot();
    skip(iter);
    if (*iter != '{')
        parse_error(iter, '{');
    iter++;
    do
    {
        JData data = parse_data(iter);
        node.nodes.emplace_back(data);
    } while (*iter != '}');
    parse_error(iter, '}');
#ifdef LOG
    LOG_WARN << "finish a obj" << END;
#endif
    return node;
}

void CXXJson::loadJson(std::string data)
{
    CXXJsonParser parser;
    str_iter iter = data.begin();
    root = parser.parse(iter);
}

JNode CXXJson::getNode(const std::string &key)
{
    JNode data;
    for (JData kv : root.nodes)
    {
        JNode node = kv.first;
        if (node.value == key)
        {
            data = kv.second;
            break;
        }
    }
    return data;
}

CXXJson CXXJson::getNodeTree(const std::string &key)
{
    CXXJson jtree;
    JNode r = getNode(key);
    jtree.root = r;
    return jtree;
}

size_t CXXJson::getSize()
{
    return root.nodes.size();
}

bool CXXJson::isEmpty()
{
    return root.isEmpty;
}

void CXXJson::saveJsonFile(const std::string &fileName)
{
    std::ofstream file(fileName, std::ios::out | std::ios::trunc);
    save(file, root);
}

void CXXJson::save(std::ofstream &file, JNode const &root)
{
    file << "{\n";
    std::cout << "{" << std::endl;
    for (auto i = root.nodes.begin(); i != root.nodes.end(); i++)
    {
        JNode keyNode = std::get<0>(*i);
        if (i->first.isEmpty)
        {
            continue;
        }
        file << '"' << std::get<0>(*i).value << "\":";
        std::cout << '"' << std::get<0>(*i).value << "\":";

        JNode valueNode = std::get<1>(*i);
        if (valueNode.isRoot)
        {
            save(file, valueNode);
            //printJSON(valueNode);
        }
        else
        {
            JValue v = valueNode.getValue();
            if (v.isNumber())
            {
                file << v.getNumber();
            }
            else
            {
                file << v.str;
            }
            std::cout << '"' << valueNode.value << '"';
        }
        if (i != root.nodes.end() - 1)
        {
            std::cout << "," << std::endl;
            file << ",\n";
        }
    }
    std::cout << "}" << std::endl;
    file << "}\n";
}

bool CXXJson::getValue(const std::string &key, std::string& value)
{
    JNode node  = getNode(key);
    JValue v = node.getValue();
    if (v.isNumber())
    {
        return false;
    }
    else
    {
        value = v.getString();
        return true;
    }

}

bool CXXJson::getValue(const std::string &key, int& value)
{
    JNode node  = getNode(key);
    JValue v = node.getValue();
    if (v.isNumber())
    {
        value = v.getNumber();
        return true;
    }
    else
    {
        return false;
    }
}

void printJSON(JNode root)
{
    std::cout << "{" << std::endl;
    for (auto i = root.nodes.begin(); i != root.nodes.end(); i++)
    {
        JNode keyNode = std::get<0>(*i);
        std::cout << '"' << std::get<0>(*i).value << "\":";
        JNode valueNode = std::get<1>(*i);
        if (valueNode.isRoot)
        {
            printJSON(valueNode);
        }
        else
        {
            std::cout << '"' << valueNode.value << '"';
        }
        if (i != root.nodes.end() - 1)
            std::cout << "," << std::endl;
    }
    std::cout << "}" << std::endl;
}