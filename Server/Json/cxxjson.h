#ifndef CXXJSON_H_
#define CXXJSON_H_
#include <string>
#include <vector>
#include <utility>
#include <fstream>

struct JNode;
typedef std::pair<JNode, JNode> JData;

struct JValue
{
    std::string str;
    int i;
    bool is_string = false;
    bool is_number = false;

    bool isString()
    {
        return is_string;
    }
    bool isNumber()
    {
        return is_number;
    }

    void setValue(int _i)
    {
        is_number = true;
        is_string = false;
        i = _i;
    }

    void setValue(const std::string &s)
    {
        is_number = false;
        is_string = true;
        str = "\"" + s + "\"";
    }

    std::string getString()
    {
        return str.substr(1, str.size()-2);
    }

    int getNumber()
    {
        return i;
    }
};

struct JNode
{
    JValue v;
    std::string value;

    bool isKey = false;
    bool isValue = false;
    bool isRoot = false;
    bool isEmpty = false;
    std::vector<JData> nodes;

    JValue getValue()
    {
        return v;
    }

    void setKey(std::string key)
    {
        isKey = true;
        value = key;
    }

    void setValue(std::string v)
    {
        isValue = true;
        value = v;
    }

    void setRoot()
    {
        isRoot = true;
    }
};

typedef std::string::iterator str_iter;
class CXXJsonParser
{
  public:
    JNode parse(str_iter &);

  private:
    JNode parse_key(str_iter &);
    JNode parse_value(str_iter &);
    JData parse_data(str_iter &);
    void skip(str_iter &);
    std::string match(str_iter &iter, char end);
    std::string matchNumber(str_iter &iter);
    void parse_error(str_iter &iter, char c);
};

class CXXJson
{
  public:
    CXXJson(const JNode &node)
    {
        root = node;
    };
    CXXJson() {}
    void loadJson(std::string data);
    JNode getNode(const std::string &key);
    CXXJson getNodeTree(const std::string &key);
    size_t getSize();
    bool isEmpty();
    void saveJsonFile(const std::string &fileName);

    bool getValue(const std::string &key, std::string &value);

    bool getValue(const std::string &key, int &value);

    bool setValue(const std::string &key, const JNode &jtree)
    {
        for (auto i = root.nodes.begin(); i != root.nodes.end(); i++)
        {
            if (i->first.value == key)
            {
                i->second.setRoot();
                i->second.nodes = jtree.nodes;
                return true;
            }
        }
        return false;
    }

    template<typename T>
    bool setValue(const std::string &key, T value)
    {
        for (auto i = root.nodes.begin(); i != root.nodes.end(); i++)
        {
            if (i->first.value == key)
            {
                if (i->first.isRoot)
                    return false;
                i->second.v.setValue(value);
                return true;
            }
        }
        return false;
    }

    JNode getNode()
    {
        return root;
    }

  private:
    void save(std::ofstream &file, const JNode &node);
    JNode root;
};

#endif