#ifndef THREAD_SAFE_QUEUE_H_
#define THREAD_SAFE_QUEUE_H_
#include <thread>
#include <queue>
template<typename T>
class ThreadSafe_Queue
{
public:
    void push(const T &item)
    {
        std::lock_guard<std::mutex> lock(mut);
        queue.push(std::move(item));
    }

    bool try_pop(T &get)
    {
        std::lock_guard<std::mutex> lock(mut);
        if (queue.empty())
        {
            return false;
        }
        get = std::move(queue.front());
        queue.pop();
        return true;
    }

    bool empty() const
    {
        std::lock_guard<std::mutex> lock(mut);
        return queue.empty();
    }

    std::queue<T> getQueue()
    {
        return queue;
    }
private:
    mutable std::mutex mut;
    std::queue<T> queue;
};
#endif
