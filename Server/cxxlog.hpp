#ifndef CXXLOG_HPP_
#define CXXLOG_HPP_
#include <stdlib.h>
#include <sys/msg.h>
#include <sys/time.h>
#include <unistd.h>

#include <cstdio>
#include <sstream>
#include <fstream>

class END {};

class CXXLOG
{

public:
    CXXLOG(int line, const char * file, const char * function, const char * level, int color)
    {
        msg += "\033[1;" + colors[color][0] + ";" + colors[color][1] + "m";
        char level_text[30];
        snprintf(level_text, 30, "[%-8s]", level);
#ifndef DISABLE_LEVEL
        msg += level_text;
#endif
        log += level_text;

        getTime();

        char file_text[80];
        snprintf(file_text, 80, "[%-10s]", file);
#ifndef DISABLE_FILE
        msg += file_text;
#endif
        log += file_text;

        char function_text[40];
        snprintf(function_text,40,"[%10s:%-4d]", function, line);
#ifndef DISABLE_FUNCTION
        msg += function_text;
#endif
        log += ":" + std::to_string(line) + ":";
        log += " : ";
        msg += "\033[0m:\t";
    }

    CXXLOG& operator<<(END)
    {
        End();
        return *this;
    }

    CXXLOG& operator<<(char *str)
    {
        log += str;
        msg += str;
        return *this;
    }

    CXXLOG& operator<<(const char *str)
    {
        log += str;
        msg += str;
        return *this;
    }

    template<typename T>
    CXXLOG& operator<<(T value)
    {
        std::stringstream ss;
        ss << value;
        log += ss.str();
        msg += ss.str();
        return *this;
    }

    CXXLOG& stream()
    {
        return *this;
    }

private:
    void getTime()
    {
        struct timeval tv;
        struct timezone tz;
        struct tm *t;

        gettimeofday(&tv, &tz);
        t = localtime(&tv.tv_sec);

        char year[6];
        snprintf(year, 6, "%d-", t->tm_year+1900);

        char month[4];
        snprintf(month, 4, "%d-", t->tm_mon+1);

        char day[4];
        snprintf(day, 4, "%d-", t->tm_mday);

        char hour[4];
        snprintf(hour, 4, "%d-", t->tm_hour);

        char min[4];
        snprintf(min, 4, "%d-", t->tm_min);

        char sec[3];
        snprintf(sec, 3, "%d", t->tm_sec);
#ifndef DISABLE_TIME
        msg += year;
        msg += month;
        msg += day;
        msg += hour;
        msg += min;
        msg += sec;
#endif
        log += "[";
        log += year;
        log += month;
        log += day;
        log += hour;
        log += min;
        log += sec;
        log += "]";
    }

    void End()
    {
#ifdef NORMAL
        std::printf("%s\n", msg.c_str());
#endif
        log += "\n";
        std::ofstream file("log", std::ios::app);
        file << log;
        file.close();
    }

private:
    std::string log;
    std::string msg;
    std::string colors[8][2] = {{"31","40"},{"37","40"},{"36","40"},{"33","40"},{"37","41"},{"37","45"},{"37","43"},{"37","44"}};
};

#define INFO __LINE__,__FILE__,__FUNCTION__
#define END END()
//level 1
#define LOG_DEBUG CXXLOG(INFO,"debug",0).stream()
#define LOG_INFO CXXLOG(INFO,"info",1).stream()
#define LOG_TRACE CXXLOG(INFO,"trace",2).stream()
#define LOG_WARN CXXLOG(INFO,"warm",3).stream()
//level 2
#define LOG_ERROR CXXLOG(INFO,"error",4).stream()
#define LOG_FATAL CXXLOG(INFO,"fatal",5).stream()
#define LOG_SYSERR CXXLOG(INFO,"syserr",6).stream()
#define LOG_SYSFATAL CXXLOG(INFO,"sysfatal",7).stream()
#endif
